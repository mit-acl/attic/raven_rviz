/*
 * waypoint.cpp
 *
 *  Created on: Jun 7, 2012
 *      Author: mark
 */

#include "waypoint.h"

enum
{
	TAKEOFF = 1,
	LAND = 2,
	MIN_VEL = 1,
	MAX_VEL = 4,
	NUM_VEL = 5,
};

// initialize server variable during construction of class
Waypoint::Waypoint(std::string name,
		interactive_markers::InteractiveMarkerServer * server) :
		menu_handler()
{
	// WARNING -- this is currently only implemented for quads
	// TODO: extend the implementation for ground vehicles as well, shouldn't be hard
	if (name.substr(0,2) == "mQ" or name.substr(0,2) == "BQ" or name.substr(0,2) == "RQ")
	{

		this->name = name;
		flight_status = LAND;
		vel_last = mode_last = 0;
		this->server = server;

		ros::NodeHandle n;
		goal_pub = n.advertise<acl_msgs::Waypoint>(name + "/quad_waypoint", 5);
		for (int i=0; i<NUM_VEL; i++)
		{
			vel_disc.push_back(MIN_VEL + (double) i/(NUM_VEL-1)*(MAX_VEL-MIN_VEL));
		}

		// subscribe to vehicle pose
		pose_sub = n.subscribe(name + "/pose", 1, &Waypoint::poseCB, this);

		createWP();
	}
}

Waypoint::~Waypoint()
{
	eraseWP();
}

void Waypoint::createWP()
{
	mode_last = menu_handler.insert("Takeoff",
			boost::bind(&Waypoint::flightModeCB, this, _1));
	menu_handler.setCheckState(mode_last,
			interactive_markers::MenuHandler::UNCHECKED);
	mode_last = menu_handler.insert("Land",
			boost::bind(&Waypoint::flightModeCB, this, _1));
	menu_handler.setCheckState(mode_last,
			interactive_markers::MenuHandler::CHECKED);
	interactive_markers::MenuHandler::EntryHandle sub_menu_handle =
			menu_handler.insert("Velocity");

	std::ostringstream s;
	s << vel_disc.at(0) << " m/s";
	vel_last = menu_handler.insert(sub_menu_handle, s.str(),
		boost::bind(&Waypoint::velocityCB, this, _1));
	menu_handler.setCheckState(vel_last,
			interactive_markers::MenuHandler::CHECKED);
	velocity = vel_disc.at(0);

	for (int i=1; i<vel_disc.size(); i++)
	{
		s.str("");
		s.clear();
		s << vel_disc.at(i) << " m/s";
		interactive_markers::MenuHandler::EntryHandle vel_tmp = menu_handler.insert(sub_menu_handle, s.str(),
			boost::bind(&Waypoint::velocityCB, this, _1));
		menu_handler.setCheckState(vel_tmp,
				interactive_markers::MenuHandler::UNCHECKED);
	}


	makeWaypointMarker(name);

	server->applyChanges();
}

Marker Waypoint::makeBox(InteractiveMarker &msg, double color[4])
{
	Marker marker;

	marker.type = Marker::CUBE;
	marker.scale.x = msg.scale * 0.45;
	marker.scale.y = msg.scale * 0.45;
	marker.scale.z = msg.scale * 0.45;
	marker.color.r = color[0];
	marker.color.g = color[1];
	marker.color.b = color[2];
	marker.color.a = color[3];
	marker.lifetime = ros::Duration(3);

	return marker;
}

void Waypoint::makeWaypointMarker(std::string name)
{
	InteractiveMarker int_marker;

	/********** WAYPOINT MARKER *************/
	int_marker.header.frame_id = "world";
	int_marker.pose.position.y = 0.0;
	int_marker.scale = 0.5;

	int_marker.name = name + "_waypoint_marker";
	int_marker.description = name + " Waypoint";

	InteractiveMarkerControl control;

	control.orientation.w = 1;
	control.orientation.x = 0;
	control.orientation.y = 1;
	control.orientation.z = 0;
	control.interaction_mode = InteractiveMarkerControl::ROTATE_AXIS;
	int_marker.controls.push_back(control);
	control.interaction_mode = InteractiveMarkerControl::MOVE_AXIS;
	int_marker.controls.push_back(control);
	int_marker.pose = marker_pose;

	// make a box which also moves in the plane
	double color[4] =
	{ 1, 1, 1, 0.0 };
	control.interaction_mode = InteractiveMarkerControl::MOVE_PLANE;
	control.markers.push_back(makeBox(int_marker, color));
	control.always_visible = false;
	int_marker.controls.push_back(control);

	server->insert(int_marker);
	server->setCallback(int_marker.name,
			boost::bind(&Waypoint::waypointCB, this, _1));

	/*********** TAKEOFF/LAND MENU ****************/
	menu_handler.apply(*server, int_marker.name);

	/*********** DUMMY VISIBLE WAYPOINT *********/
	int_marker.name = name + "_waypoint_visible";
	int_marker.description = "";
	int_marker.scale *= 0.25;
	color[0] = 0.5;
	color[1] = 1;
	color[2] = 0.5;
	color[3] = 0.3;

	control.interaction_mode = InteractiveMarkerControl::NONE;
	control.always_visible = true;

	int_marker.controls.clear();
	control.markers.clear();
	control.markers.push_back(makeBox(int_marker, color));
	int_marker.controls.push_back(control);

	server->insert(int_marker);
}

void Waypoint::eraseWP()
{
	server->erase(name + "_waypoint_marker");
	server->erase(name + "_waypoint_visible");
}

void Waypoint::flightModeCB(
		const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback)
{
	menu_handler.setCheckState(mode_last,
			interactive_markers::MenuHandler::UNCHECKED);
	mode_last = feedback->menu_entry_id;
	menu_handler.setCheckState(mode_last,
			interactive_markers::MenuHandler::CHECKED);

	waypoint_msg.header.frame_id = name;
	waypoint_msg.goal_pose = feedback->pose;
	waypoint_msg.takeoff = waypoint_msg.land = false;
	waypoint_msg.velocity = velocity;
	if (mode_last == TAKEOFF and flight_status == LAND){
		waypoint_msg.takeoff = true;
		flight_status = TAKEOFF;
		goal_pub.publish(waypoint_msg);
	}
	else if (mode_last == LAND and flight_status == TAKEOFF)
	{
		waypoint_msg.land = true;
		flight_status = LAND;
		goal_pub.publish(waypoint_msg);
	}

	menu_handler.reApply(*server);
	server->applyChanges();
}

void Waypoint::velocityCB(
		const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback)
{
	menu_handler.setCheckState(vel_last,
			interactive_markers::MenuHandler::UNCHECKED);
	vel_last = feedback->menu_entry_id;
	menu_handler.setCheckState(vel_last,
			interactive_markers::MenuHandler::CHECKED);

	// TODO: set velocity

	ROS_INFO("Switching to menu entry #%d", vel_last);

	menu_handler.reApply(*server);
	server->applyChanges();
}

void Waypoint::waypointCB(
		const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback)
{

	geometry_msgs::Pose pose = project2room(feedback->pose);

	if (feedback->event_type
			== visualization_msgs::InteractiveMarkerFeedback::MOUSE_UP and flight_status == TAKEOFF)
	{
		server->setPose(name + "_waypoint_marker", pose);
		if (flight_status == TAKEOFF)
		{
			waypoint_msg.header.frame_id = name;
			waypoint_msg.goal_pose = pose;
			waypoint_msg.takeoff = waypoint_msg.land = false;
			waypoint_msg.velocity = velocity;
			goal_pub.publish(waypoint_msg);
			ROS_INFO("Setting waypoint");
		}
	}

	if (flight_status != LAND)
		server->setPose(name + "_waypoint_visible", pose);

	server->applyChanges();
}

geometry_msgs::Pose Waypoint::project2room(geometry_msgs::Pose pose)
{
	double xmax = 2.5;
	double xmin = -8.0;
	double ymax = 3.0;
	double ymin = -7.0;
	double zmax = 1.8;
	double zmin = 0;
	double x_cor = -2.0;
    double y_cor = -1.8;
    double pole_x_min = -3.9;
    double pole_x_max = -2.6;
    double pole_y_min = -5.0;
    double pole_y_max = -3.7;

	// first, saturate to outer room bounds
	if (pose.position.x > xmax)
		pose.position.x = xmax;
	if (pose.position.x < xmin)
		pose.position.x = xmin;
	if (pose.position.y > ymax)
		pose.position.y = ymax;
	if (pose.position.y < ymin)
		pose.position.y = ymin;
	if (pose.position.z > zmax)
		pose.position.z = zmax;
	if (pose.position.z < zmin)
		pose.position.z = zmin;

	// next, check if we are in the corner
    if (pose.position.x < x_cor and pose.position.y > y_cor)
    {
    	// figure out which wall to project to
    	double xdist = fabs(x_cor - pose.position.x);
    	double ydist = fabs(pose.position.y - y_cor);

    	// if we are closer in x to the corner than in y, saturate the x dimension
    	if (xdist < ydist)
    		pose.position.x = x_cor;
    	else
    		pose.position.y = y_cor;
    }

    // now, check if we are in the pole
    if (pose.position.x < pole_x_max and pose.position.x > pole_x_min and
    		pose.position.y < pole_y_max and pose.position.y > pole_y_min)
    {
    	// figure out which edge in x and y you are closest to
    	double xdistmax = fabs(pose.position.x - pole_x_max);
    	double xdistmin = fabs(pose.position.x - pole_x_min);
    	double ydistmax = fabs(pose.position.y - pole_y_max);
    	double ydistmin = fabs(pose.position.y - pole_y_min);
    	double mindist = std::min(std::min(xdistmax, xdistmin), std::min(ydistmax, ydistmin));

    	// project onto the side that we are closest to
    	if (xdistmax == mindist)
    		pose.position.x = pole_x_max;
    	else if (xdistmin == mindist)
    		pose.position.x = pole_x_min;
    	else if (ydistmin == mindist)
    		pose.position.y = pole_y_min;
    	else if (ydistmax == mindist)
    		pose.position.y = pole_y_max;

    }

    return pose;
}

void Waypoint::poseCB(const geometry_msgs::PoseStamped& msg)
{
	if (flight_status != TAKEOFF)
	{
		// when we aren't flying, have waypoint follow actual vehicle
		geometry_msgs::Pose pose = project2room(msg.pose);
		server->setPose(name + "_waypoint_visible", pose);
		server->setPose(name + "_waypoint_marker", pose);
		server->applyChanges();
	}
}
