/*
 * RAVEN_world.cpp
 *
 *  Created on: Jun 6, 2012
 *      Author: mark
 */

#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

// Initialize a cube marker
void initMarkerCube(visualization_msgs::Marker &marker)
{
	marker.header.frame_id = "world";
	marker.header.stamp = ros::Time::now();
	marker.ns = "RAVEN_wall";
	marker.type = visualization_msgs::Marker::CUBE;
	marker.action = visualization_msgs::Marker::ADD;
	marker.lifetime = ros::Duration(3); // 3 second duration
	marker.pose.orientation.x = 0.0;
	marker.pose.orientation.y = 0.0;
	marker.pose.orientation.z = 0.0;
	marker.pose.orientation.w = 1.0;
}

// Make a wall marker
visualization_msgs::Marker makeWall(int markerID, geometry_msgs::Vector3 scale,
		std_msgs::ColorRGBA color, geometry_msgs::Point position)
{

	visualization_msgs::Marker m;
	initMarkerCube(m);

	m.id = markerID;
	m.color = color;
	m.scale = scale;
	m.pose.position = position;

	return m;
}

int main(int argc, char** argv)
{
	/**************************************************************
	 * This function broadcasts static shapes (markers)
	 * that are used to create the RAVEN world in RViz.
	 * The markers are broadcast once per second.
	 */

	// TODO: parameterize code by making RAVEN dimensions variables
	ros::init(argc, argv, "RAVEN_world");
	ros::NodeHandle n;
	ros::Rate r(1);
	ros::Publisher marker_pub = n.advertise<visualization_msgs::MarkerArray>(
			"RAVEN_world", 1);

	visualization_msgs::MarkerArray markerArray; // array for sending RAVEN marker messages

	int markerID = -1;
	geometry_msgs::Vector3 scale;
	geometry_msgs::Point position;
	std_msgs::ColorRGBA color;

	/*********  WALL 1 - back wall *********/
	markerID++;
	color.r = 139/255.0f; // Set the color -- be sure to set alpha to something non-zero!
	color.g = 119/255.0f;
	color.b = 101/255.0f;
	color.a = 1.0;

	scale.x = 0.1;
	scale.y = 11.76;
	scale.z = 2.0;

	position.x = 3.1;
	position.y = -2.15;
	position.z = scale.z / 2.0; // put marker floor on rViz floor

	markerArray.markers.push_back(makeWall(markerID, scale, color, position)); // add marker  to array

	/*********  WALL 2 - left back wall *********/
	markerID++;

	scale.x = 6.1;
	scale.y = 0.1;
	scale.z = 2.0;

	position.x = 0.05;
	position.y = 3.73;
	position.z = scale.z / 2.0; // put marker floor on rViz floor

	markerArray.markers.push_back(makeWall(markerID, scale, color, position)); // add marker to array

	/*********  WALL 3 - right wall *********/
	markerID++;

	scale.x = 12.52;
	scale.y = 0.1;
	scale.z = 2.0;

	position.x = -3.16;
	position.y = -8.05;
	position.z = scale.z / 2.0; // put marker floor on rViz floor

	markerArray.markers.push_back(makeWall(markerID, scale, color, position)); // add marker to array

	/*********  WALL 4 - desk wall *********/
	markerID++;

	scale.x = 0.1;
	scale.y = 4.31;
	scale.z = 0.5;

	position.x = -3.0;
	position.y = 1.58;
	position.z = scale.z / 2.0; // put marker floor on rViz floor

	markerArray.markers.push_back(makeWall(markerID, scale, color, position)); // add marker to array

	/*********  WALL 5 - left back wall *********/
	markerID++;

	scale.x = 6.42;
	scale.y = 0.1;
	scale.z = 0.5;

	position.x = -6.21;
	position.y = -0.58;
	position.z = scale.z / 2.0; // put marker floor on rViz floor

	markerArray.markers.push_back(makeWall(markerID, scale, color, position)); // add marker to array

	/*********  WALL 6 - front wall *********/
	markerID++;

	scale.x = 0.1;
	scale.y = 7.47;
	scale.z = 2.0;

	position.x = -9.42;
	position.y = -4.315;
	position.z = scale.z / 2.0; // put marker floor on rViz floor

	markerArray.markers.push_back(makeWall(markerID, scale, color, position)); // add marker to array

	/*********  Pillar *********/
	markerID++;

	scale.x = 3.53 - 3.0;
	scale.y = 4.5 - 3.97;
	scale.z = 2.0;

	position.x = (-3.53 - 3.0) / 2.0;
	position.y = (-4.5 - 3.97) / 2.0;
	position.z = scale.z / 2.0; // put marker floor on rViz floor

	markerArray.markers.push_back(makeWall(markerID, scale, color, position)); // add marker to array

	/*********  DESK TOP *********/
	markerID++;
	color.r = 0.5f;
	color.g = 0.5f;
	color.b = 0.5f;
	color.a = 1.0;

	scale.x = 0.9;
	scale.y = 3.2;
	scale.z = 0.1;

	position.x = (-3.95-3.15)/2.0;
	position.y = (3.0-0.1)/2.0;
	position.z = 0.5;

	markerArray.markers.push_back(makeWall(markerID, scale, color, position)); // add marker  to array

	/*********  DESK - LEG 1 *********/
	markerID++;

	scale.x = 0.1;
	scale.y = 0.1;
	scale.z = 0.5;

	position.x = -3.15;
	position.y = -0.1;
	position.z = scale.z / 2.0; // put marker floor on rViz floor

	markerArray.markers.push_back(makeWall(markerID, scale, color, position)); // add marker to array

	/*********  DESK - LEG 2 *********/
	markerID++;

	position.x = -3.15;
	position.y = 3.0;
	position.z = scale.z / 2.0; // put marker floor on rViz floor

	markerArray.markers.push_back(makeWall(markerID, scale, color, position)); // add marker to array

	/*********  DESK - LEG 3 *********/
	markerID++;

	position.x = -3.95;
	position.y = 3.0;
	position.z = scale.z / 2.0; // put marker floor on rViz floor

	markerArray.markers.push_back(makeWall(markerID, scale, color, position)); // add marker to array

	/*********  DESK - LEG 4 *********/
	markerID++;

	position.x = -3.95;
	position.y = -0.1;
	position.z = scale.z / 2.0; // put marker floor on rViz floor

	markerArray.markers.push_back(makeWall(markerID, scale, color, position)); // add marker to array

	/*********  RAVEN TEXT *********/
	markerID++;

	scale.z = 0.3;

	position.x = 3;
	position.y = -1;
	position.z = 1;

	visualization_msgs::Marker m = makeWall(markerID, scale, color, position);
	m.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
	m.text = "RAVEN";
	markerArray.markers.push_back(m);

	while (ros::ok())
	{
		/********** Publish the marker *********/
		marker_pub.publish(markerArray);

		r.sleep();
	}
}
