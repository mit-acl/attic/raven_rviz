/*
 * waypoint_main.cpp
 *
 *  Created on: Jun 7, 2012
 *      Author: mark
 */

#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <interactive_markers/interactive_marker_server.h>
#include <interactive_markers/menu_handler.h>

#include "waypoint.h"

int main(int argc, char ** argv)
{
	std::string name;
	if (argc > 1)
	{
		name = argv[1];
	}
	else
	{
		printf("Need to specify name!\n");
		return -1;
	}

	// Initialize ROS and node n -- generic name is "Quad", overwritten by name remapping from command line
	ros::init(argc, argv, name + "_waypoint", ros::init_options::NoSigintHandler);
	ros::NodeHandle n;

	interactive_markers::InteractiveMarkerServer * server;
	server = new interactive_markers::InteractiveMarkerServer("rviz", "", false);

	Waypoint wp1(argv[1], server);
	Waypoint wp2("BQ02s", server);

	ros::spin();

	ROS_INFO("Exiting..");

	return 1;
}


